using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace OurDatabase
{
    public class NodeRef<TIndex>
    {
        public TIndex Index { get; set; }
        public long RefData { get; set; }
        public long? RefLeft { get; set; }
        public long? RefRight { get; set; }
    }

    public class NodePersistent<T, TIndex> : INodePersistent
    {
        private long BlockNumber { get; }
        private TIndex Index => _nodeRef.Index;

        public T Data
        {
            get => _dataStorage.ReadBlock(_nodeRef.RefData);
            private set
            {
                if (value == null) throw new ArgumentNullException(nameof(value));
            }
        }

        public NodePersistent<T, TIndex> Left
        {
            get => _nodeRef.RefLeft == null
                ? null
                : new NodePersistent<T, TIndex>(_dataStorage, _name, (long) _nodeRef.RefLeft);
            private set
            {
                _nodeRef.RefLeft = value.BlockNumber;
                _indexStorage.UpdateBlock(BlockNumber, _nodeRef);
            }
        }

        public NodePersistent<T, TIndex> Right
        {
            get => _nodeRef.RefRight == null
                ? null
                : new NodePersistent<T, TIndex>(_dataStorage, _name, (long) _nodeRef.RefRight);
            private set
            {
                _nodeRef.RefRight = value.BlockNumber;
                _indexStorage.UpdateBlock(BlockNumber, _nodeRef);
            }
        }

        private readonly NodeRef<TIndex> _nodeRef;
        private readonly string _name;
        private string Filename => $"db_{_name}.bin";
        private readonly Storage<NodeRef<TIndex>> _indexStorage;
        private readonly Storage<T> _dataStorage;

        public NodePersistent(Storage<T> dataStorage, string name, long fromBlockNumber)
        {
            _name = name;
            _indexStorage = new Storage<NodeRef<TIndex>>(Filename);
            _dataStorage = dataStorage;
            BlockNumber = fromBlockNumber;
            _nodeRef = _indexStorage.ReadBlock(fromBlockNumber);
        }

        public NodePersistent(Storage<T> dataStorage, string name, TIndex index, long blockNumber)
        {
            _name = name;
            _indexStorage = new Storage<NodeRef<TIndex>>(Filename);
            _dataStorage = dataStorage;

            var nodeRef = new NodeRef<TIndex>()
            {
                Index = index,
                RefData = blockNumber
            };

            BlockNumber = _indexStorage.AddBlock(nodeRef);
            _nodeRef = _indexStorage.ReadBlock(BlockNumber);
        }

        private void _add(TIndex index, long blockNumber)
        {
            var newNode = new NodePersistent<T, TIndex>(_dataStorage, _name, index, blockNumber);

            if (IsLessthen(index))
            {
                if (Left == null)
                {
                    Left = newNode;
                }
                else
                {
                    Left.Add(index, blockNumber);
                }
            }
            else
            {
                if (Right == null)
                {
                    Right = newNode;
                }
                else
                {
                    Right.Add(index, blockNumber);
                }
            }
        }

        public NodePersistent<T, TIndex> Search(TIndex searchData)
        {
            if (Index?.Equals(searchData) == true)
            {
                Data = _dataStorage.ReadBlock(_nodeRef.RefData);
                return this;
            }

            return IsLessthen(searchData) ? Left?.Search(searchData) : Right?.Search(searchData);
        }

        private bool IsLessthen(TIndex value)
        {
            var type = value.GetType();
            bool isLessthen = false;

            if (type == typeof(int) || type == typeof(Int16) || type == typeof(Int32) || type == typeof(Int64))
            {
                if (int.Parse(value.ToString()!) < int.Parse(Index.ToString()!))
                    isLessthen = true;
            }
            else if (type == typeof(string))
            {
                if (string.CompareOrdinal(value.ToString(), Index.ToString()) < 0)
                    isLessthen = true;
            }

            return isLessthen;
        }

        public void Add(object index, long blockNumber)
        {
            _add((TIndex) index, blockNumber);
        }
    }

    public interface INodePersistent
    {
        void Add(object index, long blockNumber);
    }

    public interface INode
    {
        void Add(object data);
    }

    public class Node<T, TIndex> : INode
    {
        private TIndex Index { get; }
        public Node<T, TIndex> Left { get; private set; }
        public Node<T, TIndex> Right { get; private set; }

        private readonly Expression<Func<T, TIndex>> _indexKeySelector;

        private Node(T data, Expression<Func<T, TIndex>> indexKeySelector)
        {
            _indexKeySelector = indexKeySelector;
            Index = _indexKeySelector.Compile()(data);
        }

        public void Add(object data)
        {
            Add((T) data);
        }

        private void Add(T data)
        {
            var newNode = new Node<T, TIndex>(data, _indexKeySelector);

            if (IsLessthen(data))
            {
                if (Left == null)
                {
                    Left = newNode;
                }
                else
                {
                    Left.Add(data);
                }
            }
            else
            {
                if (Right == null)
                {
                    Right = newNode;
                }
                else
                {
                    Right.Add(data);
                }
            }
        }

        public Node<T, TIndex> Search(TIndex searchData)
        {
            if (Index?.Equals(searchData) == true)
            {
                return this;
            }

            return IsLessthen(searchData) ? Left?.Search(searchData) : Right?.Search(searchData);
        }

        bool IsLessthen(TIndex value)
        {
            var type = value.GetType();
            bool isLessthen = false;

            if (type == typeof(int) || type == typeof(Int16) || type == typeof(Int32) || type == typeof(Int64))
            {
                if (int.Parse(value.ToString()!) < int.Parse(Index.ToString()!))
                    isLessthen = true;
            }
            else if (type == typeof(string))
            {
                if (string.CompareOrdinal(value.ToString(), Index.ToString()) < 0)
                    isLessthen = true;
            }

            return isLessthen;
        }

        bool IsLessthen(T newData)
        {
            var newValue = _indexKeySelector.Compile()(newData);
            return IsLessthen(newValue);
        }
    }

    public class Node<T>
    {
        public T Data { get; set; }
        public Node<T> Left { get; set; }
        public Node<T> Right { get; set; }

        Func<T, T, bool> compairLessthan;

        public Node(T data, Func<T, T, bool> compairLessthan)
        {
            this.Data = data;
            this.compairLessthan = compairLessthan;
        }

        public void Add(T data)
        {
            var newNode = new Node<T>(data, compairLessthan);
            if (compairLessthan(Data, data))
            {
                if (Left == null)
                {
                    Left = newNode;
                }
                else
                {
                    Left.Add(data);
                }
            }
            else
            {
                if (Right == null)
                {
                    Right = newNode;
                }
                else
                {
                    Right.Add(data);
                }
            }
        }

        public Node<T> Search(T searchData)
        {
            if (Data?.Equals(searchData) == true)
            {
                return this;
            }

            return compairLessthan(Data, searchData) ? Left?.Search(searchData) : Right?.Search(searchData);
        }

        public Node<T> Search(Predicate<T> predicate)
        {
            if (predicate(Data))
            {
                return this;
            }

            return Left?.Search(predicate) ?? Right?.Search(predicate);
        }
    }


    public static class NodeExtension
    {
        public static void Travel<T>(this Node<T> node, Action<Node<T>> handle)
        {
            if (node == null)
            {
                return;
            }

            node.Left.Travel(handle);
            handle(node);
            node.Right.Travel(handle);
        }

        public static void Travel<T, TIndex>(this Node<T, TIndex> node, Action<Node<T, TIndex>> handle)
        {
            if (node == null)
            {
                return;
            }

            node.Left.Travel(handle);
            handle(node);
            node.Right.Travel(handle);
        }

        public static void Travel<T, TIndex>(this NodePersistent<T, TIndex> node,
            Action<NodePersistent<T, TIndex>> handle)
        {
            if (node == null)
            {
                return;
            }

            node.Left.Travel(handle);
            handle(node);
            node.Right.Travel(handle);
        }

        public static List<Node<T>> ToList<T>(this Node<T> node)
        {
            var list = new List<Node<T>>();
            node.Travel(item => { list.Add(item); });

            return list;
        }
    }
}