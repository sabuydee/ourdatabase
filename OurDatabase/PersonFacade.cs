using System;
using System.Linq.Expressions;

namespace OurDatabase
{
    public class PersonFacade
    {
        private readonly Table<Person> _table = new Table<Person>();

        public void Add()
        {
            Console.Write("Input data of people (id, name, age) : ");
            var input = Console.ReadLine();
            var data = input!.Split(',');
            var people = new Person()
            {
                Id = int.Parse(data[0]),
                Name = data[1],
                Age = int.Parse(data[2])
            };

            _table.Add(people);
        }

        public void List<TKey>(Expression<Func<Person, TKey>> keySelector)
        {
            _table.ToList(keySelector).ForEach(e => { e.Show(); });
        }

        public void Search()
        {
            Console.WriteLine();
            Console.Write("Search by 1 = Name, 2 = Age : ");
            var choose = Console.ReadLine();

            Console.Write("Input data : ");
            var input = Console.ReadLine();

            Person result = null;

            switch (choose)
            {
                case "1":
                {
                    result = _table.Search(k => k.Name, input);
                    break;
                }
                case "2":
                {
                    var data = int.Parse(input!);
                    result = _table.Search(k => k.Age, data);
                    break;
                }
            }

            Console.WriteLine("Found : " + ((result != null) ? "yes" : "no"));
            result?.Show();
            Console.WriteLine();
        }
    }
}