using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace OurDatabase
{
    public class Table<T>
    {
        private readonly Dictionary<string, INodePersistent> _nodes = new Dictionary<string, INodePersistent>();
        string filename => $@"db_{typeof(T).Name}_data.bin".ToLower();
        readonly Storage<T> _dataStorage;

        public Table()
        {
            _dataStorage = new Storage<T>(filename);

            if (_dataStorage.IsExistData())
            {
                foreach (var prop in typeof(T).GetProperties())
                {
                    var indexName = $"{typeof(T).Name}_{prop.Name}_index".ToLower();
                    var constructedClass = typeof(NodePersistent<,>).MakeGenericType(typeof(T), prop.PropertyType);

                    _nodes[prop.Name] =
                        (INodePersistent) Activator.CreateInstance(constructedClass, _dataStorage, indexName, 0);
                }
            }

            _dataStorage.OnBlockAdded += (blockNumber, data) =>
            {
                foreach (var prop in data.GetType().GetProperties())
                {
                    var indexName = $"{typeof(T).Name}_{prop.Name}_index".ToLower();
                    var indexValue = prop.GetValue(data);

                    if (!_nodes.ContainsKey(prop.Name))
                    {
                        var constructedClass = typeof(NodePersistent<,>).MakeGenericType(typeof(T), prop.PropertyType);

                        _nodes[prop.Name] =
                            (INodePersistent) Activator.CreateInstance(constructedClass, _dataStorage, indexName,
                                indexValue, blockNumber);
                    }
                    else
                    {
                        _nodes[prop.Name].Add(indexValue, blockNumber);
                    }
                }
            };
        }

        public void Add(T data)
        {
            _dataStorage.AddBlock(data);
        }

        public List<T> ToList<TKey>(Expression<Func<T, TKey>> keySelector)
        {
            var name = ((MemberExpression) keySelector.Body).Member.Name;
            if (!_nodes.ContainsKey(name)) return new List<T>();

            var node = (NodePersistent<T, TKey>) _nodes[name];

            var list = new List<T>();
            node.Travel(p => { list.Add(p.Data); });
            return list;
        }

        public T Search<TKey>(Expression<Func<T, TKey>> keySelector, TKey value)
        {
            if (_nodes.Count == 0) return default;

            var name = ((MemberExpression) keySelector.Body).Member.Name;
            var rootNode = (NodePersistent<T, TKey>) _nodes[name];
            var node = rootNode.Search(value);
            return node == null ? default : node.Data;
        }
    }
}