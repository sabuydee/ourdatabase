﻿using System;

namespace OurDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            RunPersonProgram();
        }

        private static void RunPersonProgram()
        {
            PersonFacade facade = new PersonFacade();

            Menu:
            Console.WriteLine("---------- Menu ----------");
            Console.WriteLine("1. Add");
            Console.WriteLine("2. List");
            Console.WriteLine("3. Search");
            Console.WriteLine("Q. Quit");
            Console.Write("Chose : ");

            var cmd = Console.ReadLine();

            switch (cmd)
            {
                case "1":
                    facade.Add();
                    goto Menu;
                case "2":
                    facade.List(k => k.Id);
                    goto Menu;
                case "3":
                    facade.Search();
                    goto Menu;
                case "Q": break;
                default: goto Menu;
            }
        }
    }
}