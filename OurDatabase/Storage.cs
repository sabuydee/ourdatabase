using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static System.String;
using static System.Text.Encoding;

namespace OurDatabase
{
    public class Storage<T>
    {
        public event Action<long, T> OnBlockAdded = (blockNumber, data) => { };

        private readonly string _path;

        private readonly Dictionary<Type, int> _mapTypeLength = new Dictionary<Type, int>()
        {
            {typeof(int), 8},
            {typeof(int?), 8},
            {typeof(long), 8},
            {typeof(long?), 8},
            {typeof(string), 255},
        };

        private int BlockLength => typeof(T).GetProperties().Sum(e => _mapTypeLength[e.PropertyType]);

        // private Stream _stream;

        public Storage(string filename)
        {
            var directory = Path.Join(Environment.CurrentDirectory, "AppData");
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }

            _path = Path.Join(directory, filename);

            // if (_stream == null)
            //     _stream = File.Open(_path, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
        }

        public bool IsExistData() => File.Exists(_path);

        // blockNumber start at 0
        public T ReadBlock(long blockNumber)
        {
            var blockStartOffset = blockNumber * BlockLength;
            var blockData = new byte[BlockLength];

            using (var reader = new BinaryReader(File.Open(_path, FileMode.Open)))
            {
                reader.BaseStream.Seek(blockStartOffset, SeekOrigin.Begin);
                reader.Read(blockData, 0, BlockLength);
            }

            // _stream.Position = blockStartOffset;
            // _stream.Read(blockData, 0, BlockLength);

            return ConvertToObj(blockData);
        }


        public long AddBlock(T data)
        {
            var block = ConvertToByte(data);

            using (var stream = File.Open(_path, FileMode.Append))
            {
                // _stream.Position = _stream.Length;
                stream.Write(block, 0, block.Length);
            }


            var totalBlock = (new FileInfo(_path).Length / BlockLength) - 1;

            OnBlockAdded(totalBlock, data);

            return totalBlock;
        }

        public void UpdateBlock(long blockNumber, T data)
        {
            var blockStartOffset = blockNumber * BlockLength;
            var block = ConvertToByte(data);

            using (var stream = File.Open(_path, FileMode.Open))
            {
                stream.Position = blockStartOffset;
                stream.Write(block, 0, block.Length);
            }

            // _stream.Position = blockStartOffset;
            // _stream.Write(block, 0, block.Length);
            // _stream.Flush();

            ReadBlock(blockNumber);
        }

        private T ConvertToObj(byte[] blockData)
        {
            var objVal = Activator.CreateInstance<T>();
            var offset = 0;
            foreach (var prop in typeof(T).GetProperties())
            {
                var fieldLength = _mapTypeLength[prop.PropertyType];
                var fieldValue = new byte[fieldLength];

                Array.Copy(blockData, offset, fieldValue, 0, fieldValue.Length - 1);

                var data = ASCII.GetString(fieldValue).TrimEnd(new[] {(char) 0});

                if (prop.PropertyType == typeof(int) || prop.PropertyType == typeof(int?))
                {
                    var val = int.TryParse(data, out var valTmp) ? valTmp : (int?) null;
                    prop.SetValue(objVal, val);
                }
                else if (prop.PropertyType == typeof(long) || prop.PropertyType == typeof(long?))
                {
                    var val = long.TryParse(data, out var valTmp) ? valTmp : (long?) null;
                    prop.SetValue(objVal, val);
                }
                else if (prop.PropertyType == typeof(string))
                {
                    prop.SetValue(objVal, data);
                }

                offset += fieldLength - 1;
            }

            return objVal;
        }

        private byte[] ConvertToByte(T data)
        {
            var block = new byte[BlockLength];
            var offset = 0;
            foreach (var prop in data.GetType().GetProperties())
            {
                var fieldLength = _mapTypeLength[prop.PropertyType];
                var fieldValue = prop.GetValue(data);

                var val = ASCII.GetBytes(fieldValue?.ToString() ?? Empty);
                Array.Copy(val, 0, block, offset, val.Length);

                offset += fieldLength - 1;
            }

            return block;
        }
    }
}